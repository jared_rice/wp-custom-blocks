<?php
/**
 * Plugin Name: Fitzooth Blocks
 * Plugin URI: https://bitbucket.org/jared_rice/fitzooth-custom-blocks
 * Description: Custom Gutenberg blocks for blogging
 * Version: 0.0.4
 * Author: Jared Rice
 * Author URI: https://jared-rice.com
 *
 * @category Gutenberg
 * @author Jared Rice
 * @version 1.0
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;
function fitzooth_blocks_editor_assets(){
	$url = untrailingslashit( plugin_dir_url( __FILE__ ) );
	
    // Scripts.
    wp_enqueue_script(
        'fitzooth-blocks-js', // Handle.
        $url . '/build/index.js',
        array( 'wp-blocks', 'wp-i18n', 'wp-element' )
    );
    // Styles.
    wp_enqueue_style(
        'fitzooth-blocks-editor-css', // Handle.
        $url . '/build/editor.css',
        array( 'wp-edit-blocks' )
    );
} // End function my_custom_block_editor_assets().
// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'fitzooth_blocks_editor_assets' );

function fitzooth_blocks_assets(){
	$url = untrailingslashit( plugin_dir_url( __FILE__ ) );
	
	wp_enqueue_style(
        'fitzooth-blocks-frontend-css', // Handle.
        $url . '/build/style.css'
    );
}
// Hook: Frontend assets.
add_action( 'enqueue_block_assets', 'fitzooth_blocks_assets' );