/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { toggleFormat } = wp.richText;
const { RichTextToolbarButton, RichTextShortcut } = wp.editor;
const { registerFormatType } = wp.richText;

/**
 * Block constants
 */
const name = 'fitzooth/inlinequote';

export const inlinequote = {
    name,
    title: __('Inline Quote'),
    tagName: 'q',
    className: 'fb_inline-quote',
    edit({ isActive, value, onChange }) {
        const onToggle = () => {
            onChange(
                toggleFormat(value, {
                    type: name
                })
            );
        };
        return (
            <Fragment>
                <RichTextShortcut
                    type="primary"
                    character="e"
                    onUse={onToggle}
                />
                <RichTextToolbarButton
                    icon="editor-quote"
                    title={__('Inline Quote')}
                    onClick={onToggle}
                    isActive={isActive}
                    shortcutType="primary"
                    shortcutCharacter="e"
                />
            </Fragment>
        );

    },
};

function registerFormats() {
    [
        inlinequote,
    ].forEach(({ name, ...settings }) => registerFormatType(name, settings));
};
registerFormats();